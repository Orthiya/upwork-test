<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function create(Request $request)
    {
        $name = $request->name;
        $description = $request->description;

        if ($request->file('image')) {
            $imagePath = $request->file('image');
            $imageName = $imagePath->getClientOriginalName();

            $path = $request->file('image')->storeAs('uploads', $imageName, 'public');
        }

        $user =new User();
        $user->name =$name;
        $user->description =$description;
        $user->image_location = 'storage/'.$path;

        if($user->save())
        {
            return view('display')->with('user', $user);
        }
    }

}

